<div class="mobile clearfix">
<ul>
	@inject('pageService', 'App\Services\PageService')
	@foreach($pageService->getPages() as $page)
		@if($page['page']->slug!="triennial-2019")
		<li>
			<a href="#" alt="{{ url($page['page']->link) }}">
				<span class="cat"><span class="ar">{{ $page['page']->name_ar }}</span><br/>{{ $page['page']->name }}</span>
			</a>
			<ul>
				@foreach($page['children'] as $child)

					@if($child->slug!='open-call-exhibition-designer')
						<li><a href="{{ url($child->link) }}"><span class="ar">{{ $child->name_ar }}</span><br/>{{ $child->name }}</a></li>
					@endif

					@if($child->slug=='sat-talks-architecture')
						<?php $research = $pageService->getPageBySlug('research'); ?>
						@if($research->active==1)
							<li><a href="{{ url('pages/research') }}"><span class="ar">أبحاث</span><br/>Research</a></li>
						@else
							<li><a href="{{ url('research') }}"><span class="ar">أبحاث</span><br/>Research</a></li>
						@endif
					@endif


					@if($child->slug=='open-call-exhibition-designer')
								<?php $research = $pageService->getPageBySlug('opportunitiesi'); ?>
								@if($opportunitiesi->active==1)
									<li><a href="{{ url('pages/about/open-call-exhibition-designer') }}"><span class="ar">فرص العمل</span><br/>Opportunities</a></li>
								@else
									<li><a href="{{ url('pages/about/open-call-exhibition-designer') }}"><span class="ar">فرص العمل</span><br/>Opportunities</a></li>
								@endif




								
							@endif
				@endforeach
			</ul>
		</li>
{{--		@if($page['page']->slug=="programs")--}}
{{--			<li>--}}
{{--				<a href="#" alt="{{ url('/research') }}">--}}
{{--					<span class="cat"><span class="ar">أبحاث</span><br/>Research</span>--}}
{{--				</a>--}}
{{--			</li>--}}
{{--		@endif--}}
		@endif
	@endforeach
			<li>
			<a target="_blank" href="#" alt="#" sstyle="pointer-events: none;">
				<span class="cat"><span class="ar">إصدار ترينالي</span><br/>Triennial Edition </span>
			</a>
			<ul>
				<li class="nav-item">	<a target="_blank" href="https://rfgen.net/" class="mainlink"><span class="ar">ترينالي 2019</span><br/>Triennial 2019 </a>
				</li>
				<li class="nav-item">	<a href="{{ url('/pages/triennial-2023') }}" class="mainlink"><span class="ar">ترينالي 2023</span><br/>Triennial 2023</a>
				</li>

		</ul>
			
		</li>
	</ul>
		<div class="menu-mobile-back"><a href="#" id="mobile-menu-back"><span class="ar">رجوع</span><br/>BACK</a></div></div>