<div class="v-content nav-section desk">
	<div class="">
		<ul class="nav">
			@inject('pageService', 'App\Services\PageService')
			
			@foreach($pageService->getPages() as $page)
				@if($page['page']->slug!="triennial-2019")
					<li>
						
						<a href="{{ url($page['page']->link) }}" class="mainlink"><span class="ar">{{ $page['page']->name_ar }}</span><br/>{{ $page['page']->name }}</a>
						@if(count($page['children']))<ul class="sub english-nav">@endif
							
						@foreach($page['children'] as $child)



							
						




							@if($child->slug!='open-call-exhibition-designer')
								<li><a href="{{ url($child->link) }}"><span class="ar">{{ $child->name_ar }}</span><br/>{{ $child->name }}</a></li>
								
							@endif

							@if($child->slug=='sat-talks-architecture')
								<?php $research = $pageService->getPageBySlug('research'); ?>
								@if($research->active==1)
									<li><a href="{{ url('pages/research') }}"><span class="ar">أبحاث</span><br/>Research</a></li>
								@else
									<li><a href="{{ url('research') }}"><span class="ar">أبحاث</span><br/>Research</a></li>
								@endif




								
							@endif
							@if($child->slug=='open-call-exhibition-designer')
								<?php $research = $pageService->getPageBySlug('opportunitiesi'); ?>
								@if($opportunitiesi->active==1)
									<li><a href="{{ url('pages/about/open-call-exhibition-designer') }}"><span class="ar">فرص العمل</span><br/>Opportunities</a></li>
								@else
									<li><a href="{{ url('pages/about/open-call-exhibition-designer') }}"><span class="ar">فرص العمل</span><br/>Opportunities</a></li>
								@endif




								
							@endif

							
							



					
						@endforeach
							
						@if(count($page['children']))</ul>@endif
					</li>
					
				@endif

{{--				@if($page['page']->slug=="programs")--}}
{{--					<li>--}}
{{--						<a href="{{ url('/research') }}" class="mainlink"><span class="ar">أبحاث</span><br/>Research</a>--}}
{{--					</li>--}}
{{--				@endif--}}
		
		


@endforeach


							@if($child->slug!='opportunitiesi')
							<li><a href="{{ url($child->link) }}"><span class="ar">{{ $child->name_ar }}</span><br/>{{ $child->name }}</a></li>
							
						@endif
						
							@if($child->name=='opportunitiesi')
								<?php $opp = $pageService->getPageBySlug('opportunitiesi'); ?>
								@if($opp->active==1)
									<li><a href="{{ url('pages/research') }}"><span class="ar">jjjj</span><br/>Research</a></li>
								@else
									<li><a href="{{ url('research') }}"><span class="ar">yyyy</span><br/>Research</a></li>
								@endif
							@endif
				<li>
								<a target="_blank" href="#" class="mainlink"><span class="ar">إصدار ترينالي</span><br/>Triennial Edition</a>

					<ul>
						<li class="nav-item">	<a target="_blank" href="https://rfgen.net/" class="mainlink"><span class="ar">ترينالي 2019</span><br/>Triennial 2019 </a>
						</li>
						<li class="nav-item">	<a href="{{ url('/pages/triennial-2023') }}" class="mainlink"><span class="ar">ترينالي 2023</span><br/>Triennial 2023</a>
						</li>

					</ul>
			</li>

		</ul>
	</div>
</div>