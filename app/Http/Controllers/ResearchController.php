<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Post;
use App\Models\Repository;
use App\Models\RepositoryType;
use App\Models\ResearchBuilding;
use App\Models\ResearchContent;
use App\Models\ResearchFeedback;
use App\Models\ResearchType;
use Faker\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Psy\Util\Str;

class ResearchController extends Controller
{
    public function __construct(ResearchBuilding $model)
    {
        $this->model = $model;
    }

    public function index(){

        if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' ||
                $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $redirect);
        }

        $data = $this->model->get();
        $types = ResearchType::get();
        $repositories = Repository::get();
        $repositoryTypes = RepositoryType::get();
        $timelineContent = [];

        $timelines = ResearchContent::whereIn('slug',['pre-1960','1960-1980','1981-2000','2001-2020','post-2020'])->get();

        foreach($timelines as $c)
            $timelineContent[$c->slug] = $c;

        $conts = ResearchContent::get();

        foreach($conts as $c)
            $content[$c->slug] = $c;

        $lang = 'en';

        if(isset($_GET['lang']))
            if($_GET['lang']=='ar')
                $lang = 'ar';

        return view('pages.research.index',compact('data','types','repositories','repositoryTypes','content','timelineContent','lang'));
    }

    public function single($slug){
        $data = $this->model->where('slug',$slug)->first();
        return view('pages.research.single',compact('data'));
    }

    public function submit(Request $request){
        $data = $request->input();
        $data['ip'] = $request->ip();

        if(ResearchFeedback::create($data))
            return 1;

        return 0;
    }

    public function getData($id){
        $target = $this->model->where('id',$id)->first();
        $thumb = $target->thumb;
        $data = $target->toArray();
        $data['slides'] = $thumb;
        $data['gallery'] = $target->images;

        return json_encode($data);
    }
}
