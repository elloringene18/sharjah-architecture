<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Space;
use App\Models\SpaceImageSlide;
use App\Models\PublicationImageSlide;
use App\Models\Opportunities;
use App\Models\OpportunitiesEvent;
use App\Models\OpportunitiesImageSlides;
use App\Models\Upload;
use App\Services\Uploaders\ExternalFileUploader;
use App\Services\Uploaders\SpaceImagesUploader;
use App\Services\Uploaders\SpaceLandscapeImageUploader;
use App\Services\Uploaders\Triennial2023EventImagesUploader;
use App\Services\Uploaders\Triennial2023EventLandscapeImageUploader;
use App\Services\Uploaders\Triennial2023ImagesUploader;
use App\Services\Uploaders\Triennial2023LandscapeImageUploader;
use App\Traits\CanCreateSlug;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;

class OpportunitiesController extends Controller
{
    use CanCreateSlug;

    public function __construct(Opportunities $model, Triennial2023ImagesUploader $uploader, Triennial2023LandscapeImageUploader $luploader, ExternalFileUploader $file_uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->luploader = $luploader;
        $this->file_uploader = $file_uploader;
    }

    public function index(){
        $data = $this->model->get();
        return view('admin.triennial-2023.show',compact('data'));
    }

    public function show(){
        
        $page = Page::with('parent','posts')->where('id','8')->first();
        
        $publications = Opportunities::orderBy('created_at','DESC')->get();

        if(isset($_GET['sort']) && isset($_GET['order']) && isset($_GET['series'])){

            if($_GET['series']=="all")
                $data = Opportunities::where('active',1)->orderBy($_GET['sort'],$_GET['order'])->get();
            else {
                $data = Opportunities::where('active',1)->where('series',$_GET['series'])->orderBy($_GET['sort'],$_GET['order'])->get();
            }

            return view('pages.opportunities',compact('page','data','publications'));
        }

        $data = Opportunities::where('active',1)->whereDate('publish_date', '<', date('Y-m-d').' 00:00:00')->orderBy('id','DESC')->get();

        $upcoming = Opportunities::where('active',1)->whereDate('publish_date', '>=', date('Y-m-d').' 00:00:00')->orderBy('id','DESC')->get();

        return view('pages.opportunities',compact('page','data','upcoming'));
    }

    public function preview($id){
        $post = $this->model->find($id);
        $similar = Opportunities::where('active',1)->where('slug','!=',$post->slug)->where('series',$post->series)->get();
        return view('pages.triennial-2023-event-preview',compact('post','similar'));
    }

    public function single($slug){
        $post = Opportunities::where('slug', $slug)->first();
        $page = Page::where('slug','triennial-2023')->first();
        $similar = Opportunities::where('active',1)->where('slug','!=',$slug)->limit(2)->where('series',$post->series)->get();

        return view('pages.triennial-2023-event',compact('page','post','similar'));
    }

    public function edit($id){
        $page = $this->model->find($id);

        $pageType = [];

        if($page->externalFiles()->where('language','en')->count()){
            $pageType['en']['type'] = "file";
            $pageType['en']['value'] = $page->externalFiles()->where('language','en')->first()->uploads()->first();
        }
        elseif($page->externalLinks()->where('language','en')->count()){
            $pageType['en']['type'] = "url";
            $pageType['en']['value'] = $page->externalLinks()->where('language','en')->first();
        }

        if($page->externalFiles()->where('language','ar')->count()){
            $pageType['ar']['type'] = "file";
            $pageType['ar']['value'] = $page->externalFiles()->where('language','ar')->first()->uploads()->first();
        }
        elseif($page->externalLinks()->where('language','ar')->count()){
            $pageType['ar']['type'] = "url";
            $pageType['ar']['value'] = $page->externalLinks()->where('language','ar')->first();
        }

        return view('admin.triennial-2023.edit',compact('page','pageType'));
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function create(){
        return view('admin.triennial-2023.create');
    }

    public function store(Request $request){

        $data = $request->except('images','external','buttonLink','others');
        $data['slug'] = $this->generateSlug($request->input('title'));
        $data['publish_date'] = strtotime($request->input('publish_date'));

        $newPage = $this->model->create($data);

        $files = $request->file('images');
        $images = $request->file('slides');
        $captions = $request->input('captions');

        if($newPage){
            foreach ($files as $index=>$file){
                if($file['square'] || $file['landscape'])
                    $slide = $newPage->sliders()->create([]);

                if($file['square']){
                    // Square Image
                    $photo = ($files != null ? $this->uploader->upload($file['square']) : false);
                    $photo[0]['caption'] = $captions[$index]['EN'];
                    $photo[0]['caption_ar'] = $captions[$index]['AR'];
                    $slide->uploads()->create($photo[0]);
                }

                if($file['landscape']) {
                    // Landscape Image
                    $photo = ($files != null ? $this->luploader->upload($file['landscape']) : false);
                    $photo[0]['caption'] = $captions[$index]['EN'];
                    $photo[0]['caption_ar'] = $captions[$index]['AR'];
                    $slide->uploads()->create($photo[0]);
                }
            }

            if($request->input('others'))
                $newPage->links()->create($request->input('others'));
        }

        $page_type_en = $request->input('external')['en']['type'];
        $page_type_ar = $request->input('external')['ar']['type'];

        if($page_type_ar=="file"){
            $fileRow = $newPage->externalFiles()->create(['language'=>'ar']);
            $files = $request->file('external_file_ar');
            $photo = ($files != null ? $this->file_uploader->upload($files) : false);

            $fileRow->uploads()->create($photo[0]);
        }
        elseif($page_type_ar=="url" || $page_type_ar=="blank")
            $newPage->externalLinks()->create(['language'=>'ar','url'=>$request->input('external')['ar']['value']]);

        if($page_type_en=="file"){
            $fileRow = $newPage->externalFiles()->create(['language'=>'en']);
            $files = $request->file('external_file_en');
            $photo = ($files != null ? $this->file_uploader->upload($files) : false);

            $fileRow->uploads()->create($photo[0]);
        }
        elseif($page_type_en=="url" || $page_type_en=="blank")
            $newPage->externalLinks()->create(['language'=>'en','url'=>$request->input('external')['en']['value']]);
//
//
//        if($request->input('form_id')){
//            $newPage->forms()->delete();
//            $newPage->forms()->create(['form_id'=>1]);
//        }

        $buttonLinks = $request->input('buttonLink');

        if($buttonLinks['title'] && $buttonLinks['value'] && $buttonLinks['title_ar'] && $buttonLinks['value_ar']){
            $newPage->buttonLinks()->create($buttonLinks);
        }

        return redirect()->to('admin/triennial-2023/'.$newPage->id.'/edit');
    }

    public function update(Request $request){

        $page = $this->model->find($request->input('id'));

        if(!$page)
            dd('Page does not exist');

        $data = $request->except('images','id','buttonLink');

        if($data['title'] != $page->title)
            $data['slug'] = $this->generateSlug($request->input('title'));

        $data['publish_date'] = strtotime($request->input('publish_date'));

        $page->update($data);

        $files = $request->file('images');
        $uploads = $request->file('uploads');
        $newUploads = $request->file('newUploads');
        $captions = $request->input('captions');
        $uploadCaptions = $request->input('upload-captions');

        if($request->has('uploads')){
            $uploads = $request->input('uploads');

            foreach ($uploads as $upload){
                $target = Upload::find($upload['id']);

                if($target){
                    $target->update(['caption'=>$upload['EN'],'caption_ar'=>$upload['AR']]);
                }
            }
        }

        if($page){
            $uploads = $request->file('uploads');
            if($uploads){

                foreach ($request->file('uploads') as $uploadid => $upload){
                    if($upload){
                        $target = Upload::find($uploadid);

                        if($target){
                            $targetSlide = OpportunitiesImageSlides::find($target->uploadable_id);

                            if($target->template=="square")
                                $photo = ($files != null ? $this->uploader->upload($upload) : false);
                            else
                                $photo = ($files != null ? $this->luploader->upload($upload) : false);

                            $newUpload = $targetSlide->uploads()->create($photo[0]);

                            $uploadCaptions[$newUpload->id] = $uploadCaptions[$target->id];

                            unset($uploadCaptions[$target->id]);
                            $target->delete();
                        } else {

                        }
                    }
                }
            }

            foreach ($files as $index=>$file){

                if($file['square'] || $file['landscape']){
                    $slide = $page->sliders()->create([]);

                    if($file['square']){
                        // Square Image
                        $photo = ($files != null ? $this->uploader->upload($file['square']) : false);

                        $photo[0]['caption'] = $captions[$index]['EN'];
                        $photo[0]['caption_ar'] = $captions[$index]['AR'];

                        $slide->uploads()->create($photo[0]);
                    }

                    if($file['landscape']) {
                        // Landscape Image
                        $photo = ($files != null ? $this->luploader->upload($file['landscape']) : false);

                        $photo[0]['caption'] = $captions[$index]['EN'];
                        $photo[0]['caption_ar'] = $captions[$index]['AR'];
                        $photo[1]['caption'] = $captions[$index]['EN'];
                        $photo[1]['caption_ar'] = $captions[$index]['AR'];

                        $slide->uploads()->createMany($photo);
                    }
                }
            }

            if($newUploads['square'] || $newUploads['landscape']){

                $slide = OpportunitiesImageSlides::find($request->input('newUploads')['slide_id']);

                if($newUploads['square']) {
                    // Square Image
                    $photo = ($files != null ? $this->uploader->upload($newUploads['square']) : false);
                    $slide->uploads()->create($photo[0]);
                }

                if($newUploads['landscape']) {
                    // Landscape Image
                    $photo = ($files != null ? $this->luploader->upload($newUploads['landscape']) : false);
                    $slide->uploads()->create($photo[0]);
                }
            }

            if($uploadCaptions){
                foreach ( $uploadCaptions as $id => $caption ) {
                    $target = Upload::find($id);

                    if($target)
                        $target->update(['caption'=>$caption['EN'],'caption_ar'=>$caption['AR']]);
                }
            }

            if($request->input('delete')){
                foreach ( $request->input('delete') as $item) {
                    $target = OpportunitiesImageSlides::find($item);

                    if($target){
                        if($target->square && $target->landscape)
                            $target->square->delete();
                        else
                            $target->delete();
                    }
                }
            }
        }

        $page_type_en = $request->input('external')['en']['type'];
        $page_type_ar = $request->input('external')['ar']['type'];

        if($page_type_ar=="file"){
            $page->externalLinks()->where('language','ar')->delete();
            $files = $request->file('external_file_ar');

            if($files){
                $page->externalFiles()->where('language','ar')->delete();
                $fileRow = $page->externalFiles()->create(['language'=>'ar']);
                $photo = ($files != null ? $this->file_uploader->upload($files) : false);
                $fileRow->uploads()->create($photo[0]);
            }
        }
        elseif($page_type_ar=="url" || $page_type_ar=="blank"){
            $page->externalFiles()->where('language','ar')->delete();
            $page->externalLinks()->where('language','ar')->delete();
            $url = $page_type_ar=="blank" ? "#" : $request->input('external')['ar']['value'];
            $page->externalLinks()->create(['language'=>'ar','url'=> $url] );
        }
        elseif($page_type_ar=="page") {
            $page->externalLinks()->where('language','ar')->delete();
            $page->externalFiles()->where('language','ar')->delete();
        }

        if($page_type_en=="file"){
            $page->externalLinks()->where('language','en')->delete();
            $files = $request->file('external_file_en');

            if($files){
                $page->externalFiles()->where('language','en')->delete();
                $fileRow = $page->externalFiles()->create(['language'=>'en']);
                $photo = ($files != null ? $this->file_uploader->upload($files) : false);
                $fileRow->uploads()->create($photo[0]);
            }
        }
        elseif($page_type_en=="url" || $page_type_en=="blank"){
            $page->externalFiles()->where('language','en')->delete();
            $page->externalLinks()->where('language','en')->delete();
            $url = $page_type_ar=="blank" ? "#" : $request->input('external')['en']['value'];
            $page->externalLinks()->create(['language'=>'en','url'=> $url]);
        }
        elseif($page_type_en=="page") {
            $page->externalLinks()->where('language','en')->delete();
            $page->externalFiles()->where('language','en')->delete();
        }

//        $page->forms()->delete();
//        if($request->input('form_id')){
//            $page->forms()->create(['form_id'=>1]);
//        }

        $buttonLinks = $request->input('buttonLink');

        $page->buttonLinks()->delete();
        if($buttonLinks['title'] && $buttonLinks['value'] || $buttonLinks['title_ar'] && $buttonLinks['value_ar']){
            $page->buttonLinks()->create($buttonLinks);
        }

        return redirect()->to('admin/triennial-2023/'.$page->id.'/edit');
    }
}
