<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $fillable = ['EMAIL','FNAME','LNAME','MMERGE5','MMERGE6'];
}
