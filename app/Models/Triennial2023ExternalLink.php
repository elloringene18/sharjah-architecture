<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Triennial2023ExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
