<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
