<?php

namespace App\Models\Forms;

use Illuminate\Database\Eloquent\Model;

class FormQuestionType extends Model
{
    protected $fillable = ['name','slug'];
}
