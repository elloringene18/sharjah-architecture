<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
