<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreWorkshopExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
