<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchBuildingImage extends Model
{
    protected $fillable = ['image'];
}
