<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchContentImage extends Model
{
    protected $fillable = ['image'];
}
