<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicationExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
