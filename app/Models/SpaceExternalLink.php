<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
