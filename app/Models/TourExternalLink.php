<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourExternalLink extends Model
{
    protected $fillable = ['url','language'];
}
