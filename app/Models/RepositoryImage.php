<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepositoryImage extends Model
{
    protected $fillable = ['image'];
}
