<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('title_ar')->nullable();
            $table->longText('content')->nullable();
            $table->longText('content_ar')->nullable();
            $table->string('slug')->unique();
            $table->string('color');
            $table->longText('pre-1960')->nullable();
            $table->longText('pre-1960_ar')->nullable();
            $table->longText('1960-1980')->nullable();
            $table->longText('1960-1980_ar')->nullable();
            $table->longText('1981-2000')->nullable();
            $table->longText('1981-2000_ar')->nullable();
            $table->longText('2001-2020')->nullable();
            $table->longText('2001-2020_ar')->nullable();
            $table->longText('post-2020')->nullable();
            $table->longText('post-2020_ar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('research_types');
    }
}
